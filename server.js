const express = require('express');
const app = express();
const constant = require('./constant.json');

const routes = require('./routes');

app.use('/', routes);

// app.listen(3000, () => console.log('Example app listening on port 3000!'));
module.exports = app;