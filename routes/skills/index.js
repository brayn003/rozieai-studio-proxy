var express = require('express');
var router = express.Router();
var request = require('request-promise');

var { api_key, apis_server_url, skill_server_url, skills_postman_token, client_secret, client_id } = require('../../constant.json');

router.get('/', getSkills);
router.post('/apis', postSkillApi);

function getSkills(req, res) {
  request({
    method: 'GET',
    uri: skill_server_url + 'skills/list?api_key=' + api_key,
    headers: {
      'postman-token': skills_postman_token,
      'cache-control': 'no-cache'
    },
    json: true,
  }).then((serRes) => {
    res.json(serRes);
  }).catch((err) => {
    if(typeof err.statusCode !== 'undefined') {
      res.status(err.statusCode).send(err.error);
    } else {
      res.status(500).send(err);
    }
  });
}

function postSkillApi(req, res){
  var body = req.body;
  body.client_id = client_id;
  request({
    method: 'POST',
    uri: apis_server_url + 'skill/create',
    qs: { client_secret },
    body,
    json: true
  }).then((serRes) => {
    res.json(serRes);
  }).catch((err) => {
    if(typeof err.statusCode !== 'undefined') {
      res.status(err.statusCode).send(err.error);
    } else {
      res.status(500).send(err);
    }
  }); 
}

module.exports = router;