var express = require('express');
var router = express.Router();
var request = require('request-promise');

var { client_secret, client_id, apis_server_url } = require('../../constant.json');

router.get('/', getApis);
router.post('/', postApi);

function getApis(req, res) {
  request({
    method: 'GET',
    uri: apis_server_url + 'api/get/all',
    qs: { client_secret, client_id },
    json: true
  }).then((serRes) => {
    res.json(serRes);
  }).catch((err) => {
    if(typeof err.statusCode !== 'undefined') {
      res.status(err.statusCode).send(err.error);
    } else {
      res.status(500).send(err);
    }
  });
}

function postApi(req, res) {
  var body = req.body;
  body.client_id = client_id;
  request({
    method: 'POST',
    uri: apis_server_url + 'api/register',
    qs: { client_secret },
    body,
    json: true
  }).then((serRes) => {
    res.json(serRes);
  }).catch((err) => {
    if(typeof err.statusCode !== 'undefined') {
      res.status(err.statusCode).send(err.error);
    } else {
      res.status(500).send(err);
    }
  }); 
}

module.exports = router;