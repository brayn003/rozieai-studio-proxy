var express = require('express');
var router = express.Router();
var request = require('request-promise');

var { api_key, skill_server_url, concepts_postman_token } = require('../../constant.json');

router.get('/', getConcepts);

function getConcepts(req, res) {
  request({
    method: 'GET',
    uri: skill_server_url + 'concepts/list?api_key=' + api_key,
    headers: {
      'postman-token': concepts_postman_token,
      'cache-control': 'no-cache'
    },
    json: true,
  }).then((serRes) => {
    res.json(serRes);
  }).catch((err) => {
    if(typeof err.statusCode !== 'undefined') {
      res.status(err.statusCode).send(err.error);
    } else {
      res.status(500).send(err);
    }
  });
}

module.exports = router;