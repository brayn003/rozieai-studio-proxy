const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');

const apis = require('./apis');
const skills = require('./skills');
const concepts = require('./concepts');

router.use(cors());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.get('/', (req, res) => res.send('You have reached rozie.ai proxy server, now turn around and go back!'));
router.use('/apis', apis);
router.use('/skills', skills);
router.use('/concepts', concepts);

module.exports = router;
